<?php 

// Devices index - list of all the devices

// Init
require_once('../includes/init.php');

// Require the user to be logged in before they can see this page
Auth::getInstance()->requireLogin();

// Get the paginated device-data
// @TODO

// Show the page header, then the rest of the HTML
include('../includes/header.php');

?>

<h1>Devices</h1>

<p><a href="">Add a new Device</a></p>

<table class="uk-table uk-table-hover uk-table-striped">
	<thead>
		<tr>
			<th>Network ID</th>
			<th>Inventory ID</th>
			<th>Make</th>
			<th>Serial number</th>
			<th>Warranty enddate</th>
		</tr>
	</thead>
</table>