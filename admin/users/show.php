<?php

// User admin - show a user

// Init
require_once('../../includes/init.php');

// Require the user to be logged in before they can see this page
Auth::getInstance()->requireLogin();

// Require the user to be an administrator before they can see this page
Auth::getInstance()->requireAdmin();

// Find the user or show a 404 page
$user = User::getByIdor404($_GET);

// Get the user
if(isset($_GET['id'])) {
	$user = User::findByID($_GET['id']);
}

// Show 404 if user is not found
if(!isset($user)) {
	header('HTTP/1.0 404 Not Found');
	echo '404 not Found';
	exit;
}

// Show the page header, then the rest of the HTML
include('../../includes/header.php');

?>

<h1>User</h1>

<p><a href="/admin/users">&laquo; back to users list</a></p>

<dl class="uk-description-list-horizontal">
	<dt>Name</dt>
	<dd><?php echo htmlspecialchars($user->name); ?></dd>
	<dt>Email address</dt>
	<dd><?php echo htmlspecialchars($user->email); ?></dd>
	<dt>Active</dt>
	<dd><?php echo $user->is_active ? '&#10004;' : '&#10008;'; ?></dd>
	<dt>Administrator</dt>
	<dd><?php echo $user->is_admin ? '&#10004;' : '&#10008;'; ?></dd>
</dl>

<a href="/admin/users/edit.php?id=<?php echo $user->id; ?>" class="uk-button uk-button-primary">Edit</a>
<?php if($user->id == Auth::getInstance()->getCurrentUser()->id): ?>
	Delete
<?php else: ?>
	<a href="/admin/users/delete.php?id=<?php echo $user->id; ?>" class="uk-button uk-button-danger">Delete</a>
<?php endif; ?>

<?php include('../../includes/footer.php'); ?>