# loginProject

Ein PHP-basiertes Projekt, das sich mit Authentifizierung und grundlegender User-Verwaltung beschäftigt. Das Projekt wurde erstellt nach dem **udemy-Kurs** "Login and Registration from Scratch with PHP and MySQL](https://www.udemy.com/authentication-from-scratch-with-php-and-mysql)" und ist angelehnt an das Singelton-Designpattern.

## Directory-Übersicht

* **admin/**
  Der admin Ordner enthält die Seiten die ausschließlich für aktive Administratoren Accounts zugänglich sind.
    * **users/**
      Hier befinden sich die Seiten zur Verwaltung von Usern der Seite 
* **classes/**
  Diese Directory enthält die Singelton-Klassen, sowie eine User-, Utility- und Configuration-Klasse
* **includes**
  Der includes Ordner enthält Standard-Dateien wie den footer und den header, die auf jeder Seite eingebunden werden. Außerdem liegt hier das PHP-Script das alle wichtige
