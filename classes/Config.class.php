<?php

// Configuration class

class Config {

	// Database Settings
	const DB_HOST = 'localhost';
        const DB_NAME = 'inventory';
        const DB_USER = '%%DBUSERHERE%%';
        const DB_PASS = '%%DBPASSWORDHERE%%';

	// Mailer
	const SMTP_HOST = '%%SMTPHOSTSERVER%%';
	const SMTP_USER = '%%SMTPUSER%%';
	const SMTP_PASS = '%%SMTPUSERPASSWORD%%';

}
