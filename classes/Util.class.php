<?php

// Utilities class

class Util {

	// Redirect to a different page
	public static function redirect($url) {
		header('Location: http://' . $_SERVER['HTTP_HOST'] . $url);
		exit;
	}

	// Deny access by sending and HTTP 403 header and outputting a message
	public static function denyAccess() {
		header('HTTP/1.0 403 Forbidden');
		echo '403 Forbidden';
		exit;
	}

	// Show not found page and send an HTTP 404 header
	public static function showNotFound() {

		header('HTTP/1.0 404 Not Found');
		echo "404 Not Found";
		exit;
	}
}