<?php

// Hash class

class Hash {

	// Disallow creating a new object of the class with new Hash()
	private function __construct() {}

	// Disallow cloning the class
	private function __clone() {}

	// Get Hash of the text
	public static function make($text) {
		return password_hash($text, PASSWORD_DEFAULT);
	}

	// Compare text to its hash
	public static function check($text, $hash) {
		return password_verify($text, $hash);
	}

}