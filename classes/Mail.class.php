<?php

// PHPmailer Wrapper class

class Mail {

	// Disallow creating a new object of the class with new Mail()
	private function __construct() {}

	// Disallow cloning the class
	private function __clone() {}

	// Send an email
	public static function send($name, $email, $subject, $body) {
		require dirname(dirname(__FILE__)) . '/vendor/PHPMailer/PHPMailerAutoload.php';

		$mail = new PHPMailer();

		$mail->isSMTP();
		$mail->Host = Config::SMTP_HOST;
		$mail->SMTPAuth = true;
		$mail->Username = Config::SMTP_USER;
		$mail->Password = Config::SMTP_PASS;
		$mail->SMTPSecure = 'tls';
		$mail->Port = 587;

		$mail->From = 'lukasrhartmann@yahoo.de';

		$mail->isHTML(true);

		$mail->addAddress($email, $name);
		$mail->Subject = $subject;
		$mail->Body = $body;

		if(!$mail->send()) {
			error_log($mail->ErrorInfo);
			return false;
		} else {
			return true;
		}
	}
}