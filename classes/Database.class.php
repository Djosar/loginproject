<?php

// Database class

class Database {

	// singleton connection object
	private static $_db;

	// disallow creating a new object of this class with new Database()
	private function __construct() {}

	// disallow cloning the class
	private function __clone() {}

	// Get the instance of the PDO connection
	public static function getInstance() {
		if(static::$_db === NULL) {
			$dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';charset=utf8';
			static::$_db = new PDO($dsn, Config::DB_USER, Config::DB_PASS);

			// Raise exceptions when a datasebase exception occurs
			static::$_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}

		return static::$_db;
	}

}