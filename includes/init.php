<?php

// Initialisation

// Register autloader function
spl_autoload_register('myAutoloader');

// Autoload function
function myAutoloader($className) {
	require dirname(dirname(__FILE__)) . '/classes/' . $className . '.class.php';
}

// Authorisation
Auth::init();